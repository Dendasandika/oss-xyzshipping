/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : xyzshipping

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 20/10/2021 07:42:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for jenis_pengiriman
-- ----------------------------
DROP TABLE IF EXISTS `jenis_pengiriman`;
CREATE TABLE `jenis_pengiriman`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jenis_pengiriman` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat` double NOT NULL,
  `volume` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jenis_pengiriman
-- ----------------------------
INSERT INTO `jenis_pengiriman` VALUES (1, 'Strandard', 1, 100, 10000, NULL, NULL);
INSERT INTO `jenis_pengiriman` VALUES (2, 'Express', 1, 100, 15000, NULL, NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2021_10_19_113807_create_layanan', 1);
INSERT INTO `migrations` VALUES (2, '2021_10_19_113958_create_transaksi', 1);
INSERT INTO `migrations` VALUES (3, '2021_10_19_115219_create_status_pengiriman', 1);
INSERT INTO `migrations` VALUES (4, '2021_10_19_115338_create_transaksi_status', 1);

-- ----------------------------
-- Table structure for status_pengiriman
-- ----------------------------
DROP TABLE IF EXISTS `status_pengiriman`;
CREATE TABLE `status_pengiriman`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of status_pengiriman
-- ----------------------------
INSERT INTO `status_pengiriman` VALUES (1, 'On Progress', 'Transaksi diproses', NULL, NULL);
INSERT INTO `status_pengiriman` VALUES (2, 'On Delivery', 'Transaksi sedang dikirim', NULL, NULL);
INSERT INTO `status_pengiriman` VALUES (3, 'Delivered', 'Transaksi telah sampai', NULL, NULL);

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kode_transaksi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_pengiriman` date NOT NULL,
  `nama_pengirim` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_penerima` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_penerima` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat` double NOT NULL,
  `volume` double NOT NULL,
  `total` double NOT NULL,
  `id_jenis_pengiriman` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `transaksi_id_jenis_pengiriman_foreign`(`id_jenis_pengiriman`) USING BTREE,
  CONSTRAINT `transaksi_id_jenis_pengiriman_foreign` FOREIGN KEY (`id_jenis_pengiriman`) REFERENCES `jenis_pengiriman` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
INSERT INTO `transaksi` VALUES (2, 'SS0001', '2021-10-19', 'Denda', 'Sandika', 'Kp pintusari', 13, 0, 130000, 1, '2021-10-19 14:40:12', '2021-10-19 14:40:12');
INSERT INTO `transaksi` VALUES (3, 'SS0002', '2021-08-19', 'Leo', 'Messi', 'Jalan Argentina, Bandung', 5, 0, 50000, 1, '2021-10-20 00:03:12', '2021-10-20 00:03:12');
INSERT INTO `transaksi` VALUES (4, 'SS0003', '2021-05-07', 'Neymar', 'Junior', 'Jalan Brazil, Jakarta', 1, 500, 75000, 2, '2021-10-20 00:04:10', '2021-10-20 00:04:10');
INSERT INTO `transaksi` VALUES (5, 'SS0004', '2021-10-01', 'Cesc', 'Fabregras', 'Jalan Spanyol, Surabaya', 10, 0, 100000, 1, '2021-10-20 00:06:29', '2021-10-20 00:06:29');
INSERT INTO `transaksi` VALUES (6, 'SS0005', '2021-10-11', 'Gerard', 'Pique', 'Jalan Catalunya, Depok', 12, 0, 180000, 2, '2021-10-20 00:20:25', '2021-10-20 00:20:25');
INSERT INTO `transaksi` VALUES (7, 'SS0006', '2021-10-17', 'Ansu', 'Fati', 'Jalan Spanyol, Bogor', 1, 350, 40000, 1, '2021-10-20 00:21:22', '2021-10-20 00:21:22');
INSERT INTO `transaksi` VALUES (8, 'SS0007', '2021-10-15', 'Mephis', 'Depay', 'Jalan Belanda, Sukabumi', 3, 0, 45000, 2, '2021-10-20 00:22:28', '2021-10-20 00:22:28');
INSERT INTO `transaksi` VALUES (9, 'SS0008', '2021-10-02', 'Tiago', 'Alcantara', 'Jalan Spanyol, Yogyakarta', 12, 0, 180000, 2, '2021-10-20 00:35:33', '2021-10-20 00:35:33');
INSERT INTO `transaksi` VALUES (10, 'SS0009', '2021-08-30', 'Jordi', 'Alba', 'Jalan Catalunya, Bandung', 1, 400, 40000, 1, '2021-10-20 00:36:27', '2021-10-20 00:36:27');
INSERT INTO `transaksi` VALUES (11, 'SS0010', '2021-05-10', 'Raheem', 'Sterling', 'Jalan Inggris, Malang', 3, 600, 60000, 1, '2021-10-20 00:37:32', '2021-10-20 00:37:32');

-- ----------------------------
-- Table structure for transaksi_status
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_status`;
CREATE TABLE `transaksi_status`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_transaksi` bigint(20) UNSIGNED NOT NULL,
  `id_status` bigint(20) UNSIGNED NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `transaksi_status_id_status_foreign`(`id_status`) USING BTREE,
  INDEX `transaksi_status_id_transaksi_foreign`(`id_transaksi`) USING BTREE,
  CONSTRAINT `transaksi_status_id_status_foreign` FOREIGN KEY (`id_status`) REFERENCES `status_pengiriman` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `transaksi_status_id_transaksi_foreign` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of transaksi_status
-- ----------------------------
INSERT INTO `transaksi_status` VALUES (4, 2, 1, 'Paket diproses', '2021-10-19 14:40:12', '2021-10-19 14:40:12');
INSERT INTO `transaksi_status` VALUES (5, 3, 1, 'Paket diproses', '2021-10-20 00:03:12', '2021-10-20 00:03:12');
INSERT INTO `transaksi_status` VALUES (6, 4, 1, 'Paket diproses', '2021-10-20 00:04:10', '2021-10-20 00:04:10');
INSERT INTO `transaksi_status` VALUES (7, 3, 2, 'Paket sedang berada di gudang pusat', '2021-10-20 00:04:50', '2021-10-20 00:04:50');
INSERT INTO `transaksi_status` VALUES (8, 3, 3, 'Diterima Oleh Messi', '2021-10-20 00:05:09', '2021-10-20 00:05:09');
INSERT INTO `transaksi_status` VALUES (9, 4, 2, 'Paket sedang berada di gudang pusat', '2021-10-20 00:05:22', '2021-10-20 00:05:22');
INSERT INTO `transaksi_status` VALUES (10, 4, 3, 'Diterima Oleh Junior', '2021-10-20 00:05:39', '2021-10-20 00:05:39');
INSERT INTO `transaksi_status` VALUES (11, 5, 1, 'Paket diproses', '2021-10-20 00:06:29', '2021-10-20 00:06:29');
INSERT INTO `transaksi_status` VALUES (12, 6, 1, 'Paket diproses', '2021-10-20 00:20:25', '2021-10-20 00:20:25');
INSERT INTO `transaksi_status` VALUES (13, 7, 1, 'Paket diproses', '2021-10-20 00:21:22', '2021-10-20 00:21:22');
INSERT INTO `transaksi_status` VALUES (14, 8, 1, 'Paket diproses', '2021-10-20 00:22:29', '2021-10-20 00:22:29');
INSERT INTO `transaksi_status` VALUES (15, 9, 1, 'Paket diproses', '2021-10-20 00:35:33', '2021-10-20 00:35:33');
INSERT INTO `transaksi_status` VALUES (16, 10, 1, 'Paket diproses', '2021-10-20 00:36:27', '2021-10-20 00:36:27');
INSERT INTO `transaksi_status` VALUES (17, 11, 1, 'Paket diproses', '2021-10-20 00:37:32', '2021-10-20 00:37:32');
INSERT INTO `transaksi_status` VALUES (18, 5, 2, 'Paket sedang berada di gudang pusat', '2021-10-20 00:37:57', '2021-10-20 00:37:57');
INSERT INTO `transaksi_status` VALUES (19, 6, 1, 'Paket sedang berada di gudang pusat', '2021-10-20 00:38:05', '2021-10-20 00:38:05');
INSERT INTO `transaksi_status` VALUES (20, 7, 1, 'Paket sedang berada di gudang pusat', '2021-10-20 00:38:12', '2021-10-20 00:38:12');
INSERT INTO `transaksi_status` VALUES (21, 8, 1, 'Paket sedang berada di gudang pusat', '2021-10-20 00:38:19', '2021-10-20 00:38:19');
INSERT INTO `transaksi_status` VALUES (22, 9, 2, 'Paket sedang berada di gudang pusat', '2021-10-20 00:38:31', '2021-10-20 00:38:31');
INSERT INTO `transaksi_status` VALUES (23, 8, 2, 'Paket sedang berada di gudang pusat', '2021-10-20 00:38:40', '2021-10-20 00:38:40');
INSERT INTO `transaksi_status` VALUES (24, 7, 2, 'Paket sedang berada di gudang pusat', '2021-10-20 00:38:51', '2021-10-20 00:38:51');
INSERT INTO `transaksi_status` VALUES (25, 6, 2, 'Paket sedang berada di gudang pusat', '2021-10-20 00:39:00', '2021-10-20 00:39:00');
INSERT INTO `transaksi_status` VALUES (26, 10, 2, 'Paket sedang berada di gudang pusat', '2021-10-20 00:39:09', '2021-10-20 00:39:09');
INSERT INTO `transaksi_status` VALUES (27, 11, 2, 'Paket sedang berada di gudang pusat', '2021-10-20 00:39:17', '2021-10-20 00:39:17');
INSERT INTO `transaksi_status` VALUES (28, 5, 3, 'Diterima Oleh Fabregas', '2021-10-20 00:39:38', '2021-10-20 00:39:38');
INSERT INTO `transaksi_status` VALUES (29, 6, 2, 'Diterima Oleh Pique', '2021-10-20 00:39:51', '2021-10-20 00:39:51');
INSERT INTO `transaksi_status` VALUES (30, 7, 3, 'Diterima Oleh Fati', '2021-10-20 00:40:34', '2021-10-20 00:40:34');
INSERT INTO `transaksi_status` VALUES (31, 6, 3, 'Diterima Oleh Pique', '2021-10-20 00:40:47', '2021-10-20 00:40:47');
INSERT INTO `transaksi_status` VALUES (32, 8, 3, 'Diterima Oleh  Depay', '2021-10-20 00:41:01', '2021-10-20 00:41:01');
INSERT INTO `transaksi_status` VALUES (33, 9, 3, 'Diterima Oleh  Alcantara', '2021-10-20 00:41:20', '2021-10-20 00:41:20');
INSERT INTO `transaksi_status` VALUES (34, 10, 3, 'Diterima Oleh  Alba', '2021-10-20 00:41:32', '2021-10-20 00:41:32');
INSERT INTO `transaksi_status` VALUES (35, 11, 3, 'Diterima Oleh Sterling', '2021-10-20 00:41:44', '2021-10-20 00:41:44');

SET FOREIGN_KEY_CHECKS = 1;
