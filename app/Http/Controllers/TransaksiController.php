<?php

namespace App\Http\Controllers;

use App\JenisPengiriman;
use App\ListTransaksi;
use App\Status;
use App\Transaksi;
use App\TransaksiStatus;
use Illuminate\Http\Request;
use PDO;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{

    public function index()
    {
        $data['transaksi'] = Transaksi::with(['status','jenis_pengiriman'])->get();
        $data['status_pengiriman'] = Status::all();
        return view('list.index',$data);

    }
    public function list()
    {
        $data['transaksi'] = Transaksi::with(['status','jenis_pengiriman'])->get();
        $data['status_pengiriman'] = Status::all();
        return view('list.manage',$data);

    }
    public function create()
    {
        $data['status'] = Status::all();
        $data['jenis_pengiriman'] = JenisPengiriman::all();
        return view('list.form',$data);

    }
    public function calculateHarga(Request $request)
    {
        $berat = $request->berat;
        $volume = $request->volume;
        $id_jenis_pengiriman = $request->id_jenis_pengiriman;
        
        $jenis = JenisPengiriman::find($id_jenis_pengiriman);

        $berat_bulat = ceil($berat/$jenis->berat);
        $maxVolume = $jenis->volume*$berat_bulat;
        if($volume > $maxVolume){
            $berat_bulat = ceil($volume / $jenis->volume);
        }
        $harga = $berat_bulat * $jenis->harga;
        return $harga;
    }
    public function store(Request $request){
        $request->validate([
            'nama_penerima' => 'required',
            'nama_pengirim' => 'required',
            'id_jenis_pengiriman' => 'required',
            'id_status' => 'required',
            'volume' => 'required',
            'berat' => 'required',
            'total' => 'required',
        ]);
        $data = $request->all();
        $data['kode_transaksi'] = $this->getKodeTransaksi();
        $transaksi = Transaksi::create($data);
        if($transaksi){
            TransaksiStatus::create([
                'id_transaksi' => $transaksi->id,
                'id_status' => $request->id_status,
                'keterangan' => 'Paket diproses',
            ]);
            return redirect('/transaksi')->with('success','Transaksi berhasil disimpan');
        }else{
            return redirect('/transaksi/create')->with('error','Transaksi gagal disimpan');
        }
    }
    public function update(Request $request,$id){
        $status = $request->id_status;
        $keterangan = $request->keterangan;
        $update = TransaksiStatus::create([
            'id_transaksi' => $id,
            'id_status' => $status,
            'keterangan' => $keterangan,
        ]);
        if($update){
            return redirect('/transaksi')->with('success','Status transaksi berhasil diubah');
        }else{
            return redirect('/transaksi/create')->with('error','Status transaksi gagal diubah');
        }
    }

    public function getOneTransaksi($kode){
        $transaksi = Transaksi::with(['status','jenis_pengiriman'])->where('kode_transaksi',$kode)->first();
        return response()->json([
            'status' => $transaksi ? 'Found' : 'Not Found',
            'data' => $transaksi ?? []
        ]);
    }
    public function getKodeTransaksi(){
        $kodeName = 'SS';
        $last = Transaksi::orderBy('created_at','desc')->first();
        $codeNumber = 1;
        if($last){
            $codeNumber = (int) explode($kodeName,$last->kode_transaksi)[1] +1;
        }
        $new = $kodeName.sprintf('%04d',$codeNumber);
        return $new;
    }
    public function destroy($id){
        $transaksi = Transaksi::find($id);
        if($transaksi){
            DB::beginTransaction();

            try {
                TransaksiStatus::where('id_transaksi',$id)->delete();
                $delete = $transaksi->delete();

                DB::commit();
                
                return true;
            } catch (\Exception $e) {
                DB::rollback();
                
                return false;
            }
        }
    }
}
