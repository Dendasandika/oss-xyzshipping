<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiStatus extends Model
{
    protected $table = 'transaksi_status';
    protected $fillable = ['id','id_status','id_transaksi','keterangan'];

    public function status(){
        return $this->hasOne('\App\Status','id','id_status');
    }
}
