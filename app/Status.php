<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status_pengiriman';
    protected $fillable = ['id','nama_status','keterangan'];
}
