<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $fillable = ['id_transaksi','kode_transaksi','tanggal_pengiriman','nama_pengirim','nama_penerima','alamat_penerima','total','berat','volume','id_jenis_pengiriman'];

    public function status(){
        return $this->hasMany('\App\TransaksiStatus','id_transaksi','id');
    }
    public function jenis_pengiriman(){
        return $this->hasOne('\App\JenisPengiriman','id','id_jenis_pengiriman');
    }
}
