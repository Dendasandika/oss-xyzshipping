<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPengiriman extends Model
{
    protected $table = 'jenis_pengiriman';
    protected $fillable = ['id','jenis_pengiriman','berat','volume','harga'];
}
