@extends('layouts.app')
@section('title', 'Default Page')

@push('styles')
    
@endpush
@push('js-lib')
    
@endpush

@push('js')
    
@endpush

@section('content')
    <section class="section">
        <div class="body-section">
            <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Tambah Transaksi XYZ Shipping</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('transaksi') }}" method="POST">
                            @csrf
                            <div class="form-group">
                              <label for="">Tanggal Pengiriman</label>
                              <input type="date" name="tanggal_pengiriman" id="tanggal_pengiriman" class="form-control" placeholder="" required value="{{ @old('tanggal_pengiriman') }}">
                            </div>
                            <div class="form-group">
                              <label for="">Nama Pengirim</label>
                              <input type="text" name="nama_pengirim" id="nama_pengirim" class="form-control" placeholder="" required value="{{ @old('nama_pengirim') }}">
                            </div>
                            <div class="form-group">
                              <label for="">Nama Penerima</label>
                              <input type="text" name="nama_penerima" id="nama_penerima" class="form-control" placeholder="" required value="{{ @old('nama_penerima') }}">
                            </div>
                            <div class="form-group">
                              <label for="">Alamat</label>
                              <textarea class="form-control" name="alamat_penerima" id="alamat_penerima" rows="3">{{ @old('alamat_penerima') }}</textarea>
                            </div>
                            <div class="form-group">
                              <label for="id_jenis_pengiriman">Jenis Pengiriman</label>
                              <select class="form-control" name="id_jenis_pengiriman" id="id_jenis_pengiriman" required onchange="calculateHarga()">
                                  <option value="">Pilih Jenis Pengiriman</option>
                                    @foreach ($jenis_pengiriman as $item)
                                        <option value="{{ $item->id }}">{{ $item->jenis_pengiriman }}</option>
                                    @endforeach
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="id_status">Status</label>
                              <select class="form-control" name="id_status" id="id_status" required>
                                  <option value="">Pilih Status</option>
                                    @foreach ($status as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama_status }}</option>
                                    @endforeach
                              </select>
                            </div>
                            <div class="form-group">
                                <label for="">Berat Barang (Kg)</label>
                                <input type="number" name="berat" id="berat" value="1" class="form-control" placeholder="contoh : 1" required oninput="calculateHarga()" onblur="setDefault()">
                            </div>
                            <div class="form-group">
                                <label for="">Volume Barang (cm)</label>
                                <input type="number" name="volume" id="volume" value="0" class="form-control" placeholder="contoh : 100" required oninput="calculateHarga()" onblur="setDefault()">
                            </div>
                            <div class="form-group">
                                <label for="">Total</label>
                                <input type="number" name="total" id="total" value="0" class="form-control" required >
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
@push('js')
    <script>
        function calculateHarga(){
            let berat = $('#berat').val()
            let volume = $('#volume').val()
            let id_jenis_pengiriman = $('#id_jenis_pengiriman').val()

            $.ajax({
                type: "post",
                url: "{{ url('calculate-harga') }}",
                data: {
                    berat: berat,
                    volume: volume,
                    id_jenis_pengiriman: id_jenis_pengiriman,
                    _token: '{{ csrf_token() }}'
                },
                dataType: "json",
                success: function (harga) {
                 $('#total').val(harga)   
                }
            });
        }
        function setDefault(){
            let berat = $('#berat').val()
            let volume = $('#volume').val()
            if(berat == '') $('#berat').val(1)
            if(volume == '') $('#volume').val(0)
        }
    </script>
@endpush