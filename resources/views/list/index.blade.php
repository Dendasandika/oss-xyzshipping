@extends('layouts.app')
@section('title', 'Default Page')

@push('styles')
    
@endpush
@push('js-lib')
    
@endpush

@push('js')
    
@endpush

@section('content')
    <section class="section">
        <div class="body-section">
            <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List Transaksi XYZ Shipping</h4>
                        {{-- <div class="card-header-action">
                            <a href="{{ url('transaksi/create') }}" class="btn btn-sm btn-icon icon-left btn-primary float-right " ><i
                                    class="fas fa-plus"></i> Tambah Transaksi</a>    
                        </div> --}}
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover" id="table-programkerja">
                                <thead>
                                    <tr>
                                        <th>ID Transaksi</th>
                                        <th>Tanggal Pengiriman</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Alamat Penerima</th>
                                        <th>Total Harga</th>
                                        <th>Status</th>
                                        {{-- <th>Action</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transaksi as $item)
                                    @php
                                        $status = \App\TransaksiStatus::where('id_transaksi',$item->id)->orderBy('created_at','desc')->first()
                                    @endphp
                                        <tr>
                                            <td>{{ $item->kode_transaksi }}</td>
                                            <td>{{ $item->tanggal_pengiriman }}</td>
                                            <td>{{ $item->nama_pengirim }}</td>
                                            <td>{{ $item->nama_penerima }}</td>
                                            <td>{{ $item->alamat_penerima }}</td>
                                            <td>{{ $item->total }}</td>
                                            <td>{{ $status->status->nama_status }} : {{ $status->keterangan }}</td>
                                            {{-- <td><button type="button" class="btn btn-primary update-btn btn-sm" data-id="{{ $item->id }}" data-status="{{ $status->id_status }}"><i class="fa fa-pencil"></i> Ubah Status</button></td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    
    <!-- Modal -->
    <div class="modal fade" id="modalUpdateStatus" tabindex="-1" role="dialog" aria-labelledby="modalUpdateStatus" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Ubah Status Transaksi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" id="frm" method="POST">
                    <div class="modal-body">
                        @csrf
                        
                        <div class="form-group">
                            <label for="id_status">Status</label>
                            <select class="form-control" name="id_status" id="id_status" required>
                                <option value="">Pilih Status</option>
                                @foreach ($status_pengiriman as $s)
                                    <option value="{{ $s->id }}">{{ $s->nama_status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                        <label for="">Keterangan</label>
                        <textarea class="form-control" name="keterangan" id="keterangan" rows="3" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $(function () {
            $(document).on('click','.update-btn',function(){
                let id_status = $(this).data('status')
                let id_transaksi = $(this).data('id')
                $('#id_status').val(id_status)
                $('#frm').attr('action','{{ url("transaksi") }}/'+id_transaksi+'/update')
                $('#modalUpdateStatus').modal('show')
            })
        });
    </script>
@endpush