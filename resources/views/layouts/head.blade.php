<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
<title>@yield('title', 'SIKEMAH - Sistem Informasi Kegiatan Mahasiswa')</title>
<!-- General CSS Files -->
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/css/app.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/jqvmap/dist/jqvmap.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/weather-icon/css/weather-icons.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/weather-icon/css/weather-icons-wind.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/summernote/summernote-bs4.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/izitoast/css/iziToast.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/select2/dist/css/select2.min.css">
<!-- Template CSS -->
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/css/style.css">
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/css/components.css">
<!-- Custom style CSS -->
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/css/custom.css">
<link rel='shortcut icon' type='image/x-icon' href='{{ asset ('assets') }}/backend/img/favicon.ico' />

{{-- Custom Css --}}
<link rel="stylesheet" href="{{ asset ('assets') }}/backend/css/custom.css">
@stack('styles')