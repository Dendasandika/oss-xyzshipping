<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="">
                <img alt="image" src="{{ asset ('assets') }}/backend/img/logo-xyz.png" class="header-logo" /> 
                <span class="logo-name">Shipping</span>
            </a>
        </div>
        {{-- <div class="sidebar-user">
            <div class="sidebar-user-picture">
                <img alt="image" src="">
            </div>
            <div class="sidebar-user-details">
                <div class="user-name"></div>
                <div class="user-role"></div>
            </div>
        </div> --}}
        <ul class="sidebar-menu">
            <li class="menu-header">Main</li>            
            <li>
                <a href="{{ url('/') }}" class="nav-link"><i data-feather="monitor"></i><span>List Transaksi</span></a>
            </li>
            <li>
                <a href="{{ url('/transaksi') }}" class="nav-link"><i data-feather="archive"></i><span>Manage Transaksi</span></a>
            </li>
        </ul>
    </aside>
</div>
