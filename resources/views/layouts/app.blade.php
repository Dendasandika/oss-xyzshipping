<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
</head>

<body class="light dark-sidebar">
    {{-- <div class="loader"></div> --}}
    <div>
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            @include('layouts.header')
            @include('layouts.sidebar')
            <!-- Main Content -->
            <section class="main-content">
                @yield('content')
                
            </section>
            @stack('modal')
                {{-- <footer class="main-footer">
                    @include('layouts.footer')
                </footer> --}}
        </div>
    </div>
    @include('layouts.scripts')
    @include('layouts.message')
</body>

</html>
