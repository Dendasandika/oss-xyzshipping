<script src="{{ asset ('assets') }}/backend/js/app.min.js"></script>
<script src="{{ asset('assets') }}/backend/bundles/izitoast/js/iziToast.min.js"></script>
<script src="{{ asset('assets') }}/backend/js/page/toastr.js"></script>
<script src="{{ asset('assets') }}/backend/bundles/sweetalert/sweetalert.min.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/echart/echarts.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/chartjs/chart.min.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/select2/dist/js/select2.full.min.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/jquery-selectric/jquery.selectric.min.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/datatables/datatables.min.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/jquery-ui/jquery-ui.min.js"></script>
<script src="{{ asset ('assets') }}/backend/bundles/select2/dist/js/select2.full.min.js"></script>
@stack('js-lib')
<script src="{{ asset ('assets') }}/backend/js/page/index.js"></script>
<!-- Template JS File -->
<script src="{{ asset ('assets') }}/backend/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="{{ asset ('assets') }}/backend/js/custom.js"></script>

<script>
    $(document).ready(function(){
        @if(@session('success'))
        swal(
            'Berhasil',
            '{{ session('success') }}',
            'success'
        )
        @endif

        @if(@session('error'))
        swal(
            'Gagal',
            '{{ session('error') }}',
            'warning'
        )
        @endif
    });
</script>
@stack('js')
