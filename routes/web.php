<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TransaksiController@index');
Route::get('/transaksi/create', 'TransaksiController@create');
Route::get('/transaksi', 'TransaksiController@list');
Route::post('/transaksi', 'TransaksiController@store');
Route::post('/transaksi/{id}/update', 'TransaksiController@update');
Route::delete('/transaksi/{id}/delete', 'TransaksiController@destroy');
Route::post('/calculate-harga', 'TransaksiController@calculateHarga');
