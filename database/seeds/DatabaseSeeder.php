<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(status_seeder::class);
        $this->call(jenis_pengiriman_seeder::class);
    }
}
