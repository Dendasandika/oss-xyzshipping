<?php

use App\Status;
use Illuminate\Database\Seeder;

class status_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama_status' => 'On Progress',
                'keterangan' => 'Transaksi diproses'
            ],
            [
                'nama_status' => 'On Delivery',
                'keterangan' => 'Transaksi sedang dikirim'
            ],
            [
                'nama_status' => 'Delivered',
                'keterangan' => 'Transaksi telah sampai'
            ]
        ];
        Status::insert($data);
    }
}
