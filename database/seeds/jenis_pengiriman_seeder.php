<?php

use App\JenisPengiriman;
use Illuminate\Database\Seeder;
class jenis_pengiriman_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'jenis_pengiriman' => 'Strandard',
                'berat' => 1,
                'volume' => 100,
                'harga' => 10000,
            ],
            [
                'jenis_pengiriman' => 'Express',
                'berat' => 1,
                'volume' => 100,
                'harga' => 15000,
            ],
        ];
        JenisPengiriman::insert($data);
    }
}
