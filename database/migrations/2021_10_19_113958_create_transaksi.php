<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->id();
            $table->string('kode_transaksi');
            $table->date('tanggal_pengiriman');
            $table->string('nama_pengirim');
            $table->string('nama_penerima');
            $table->string('alamat_penerima');
            $table->double('berat');
            $table->double('volume');
            $table->double('total');
            $table->unsignedBigInteger('id_jenis_pengiriman');
            $table->timestamps();
            $table->foreign('id_jenis_pengiriman')->references('id')->on('jenis_pengiriman');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
